This repo contains my docker-compose.yml for a NextCloud instance on my remote server.
On the server, I use a docker-compose.override.yml to set the password and other details
of the database. I will be connecting this container to Authelia for user authentication.
In addition, I will be ramping it up with Kubernetes in the near future to have multiple nodes instead 
of a singular point of failure.
